package com.example.amoraclient;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends Activity
{
	public final static String EXTRA_CONNECTION = "com.example.amoraclient.CONNECTION";

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.connect:
				onConnectionRequested();
				return true;
			case R.id.disconnect:
				onDisconnectionRequested();
				return true;
			case R.id.start:
				onStartPresentation();
				return true;
			case R.id.exit:
				onExitSelected();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		MenuItem item = menu.findItem(R.id.connect);
		if (null == item)
			return false;

		item.setEnabled(!connected);
		item.setVisible(!connected);

		item = menu.findItem(R.id.disconnect);
		item.setEnabled(connected);
		item.setVisible(connected);

		item = menu.findItem(R.id.start);
		item.setEnabled(connected);

		return super.onPrepareOptionsMenu(menu);
	}

	public void onConnectionRequested() {
		connected = true;
	}

	public void onDisconnectionRequested() {
		connected = false;
	}

	public void onStartPresentation() {
		Intent intent = new Intent(this, PresentationActivity.class);
		startActivity(intent);
	}

	public void onExitSelected() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.exit_app)
			.setIcon(R.drawable.exit)
			.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					finish();
				}
			})
			.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
				}
			});

		AlertDialog dialog = builder.create();
		dialog.show();
	}

	private boolean connected = false;
}
